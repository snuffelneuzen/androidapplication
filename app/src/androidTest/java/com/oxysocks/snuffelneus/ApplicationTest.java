package com.oxysocks.snuffelneus;

import android.app.Application;
import android.content.Intent;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.widget.Button;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {
    public ApplicationTest() {
        super(Application.class);
    }

    @MediumTest
    public void testToJSONFunction() {
        Measurement testMeasurement = new Measurement();

        testMeasurement.setHumidity(10);
        testMeasurement.setTemperature(10);
        testMeasurement.setLatitude(50.0);
        testMeasurement.setLongitude(50.0);
        testMeasurement.setUserSecret("UNITTEST");

        System.out.println(Measurement.toJSONString(testMeasurement));
        assertEquals("", "");
    }
}