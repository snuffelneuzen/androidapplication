package com.oxysocks.snuffelneus;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.support.v4.content.LocalBroadcastManager;
import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import android.telephony.TelephonyManager;

/**
 * Background task for the Snuffelneus application, basicly the 'core' of this app.
 * Does background polling on the bluetooth (for measurements, wakeups, etc.) and sends output
 * to the server.
 */
public class BackgroundTask extends IntentService {
    public final static String SERVICE_NAME = "BackgroundTask";
    public final static String SNUFFELNEUS = "com.oxysocks.snuffelneus.BackgroundTask.REQUEST_PROCESSED";
    public final static String MEASURMENT_REQUEST_STRING = "wakeup\n";
    private LocalBroadcastManager broadcaster;
    private Measurement latestMeasurement;

    private BluetoothAdapter bluetoothAdapter;
    private BluetoothSocket bluetoothSocket;
    private OutputStream bluetoothOutput;
    private InputStream bluetoothInput;
    private SharedPreferences sharedPreferences;

    private GPSHandler gpsHandler;

    public BackgroundTask() { super(SERVICE_NAME); }

    @Override
    protected void onHandleIntent(Intent intent) {
        runBackgroundTask();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sharedPreferences = getSharedPreferences("com.oxysocks.snuffelneus", MODE_PRIVATE);
        broadcaster = LocalBroadcastManager.getInstance(this);
    }

    /**
     * Method that handles the actual running of the background task.
     */
    private void runBackgroundTask() {
        TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        String deviceId = telephonyManager.getDeviceId();

//        Measurement testMeasurement = new Measurement();
//        testMeasurement.setUserSecret(deviceId);
//        testMeasurement.setTemperature(24.0);
//        testMeasurement.setHumidity(80.0);
//        testMeasurement.setSensorValue(300.0);
//

//
//        if(latestLocation != null) {
//           latestMeasurement.setLatitude(latestLocation.getLatitude());
//        latestMeasurement.setLongitude(latestLocation.getLongitude());
//
//            //sendBroadcast("Lat: " + latestLocation.getLatitude() + " Long: " + latestLocation.getLongitude());
//        }
//
//        String testString = Measurement.toJSONString(testMeasurement);
//        HTTPController.getInstance().sendPOST(testString);
//        sendBroadcast(testMeasurement.toString());


        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        bluetoothSocket = createBluetoothSocket(bluetoothAdapter);

        if(bluetoothSocket == null) {
            return;
        }

        try {
            bluetoothInput = bluetoothSocket.getInputStream();
            bluetoothOutput = bluetoothSocket.getOutputStream();
        }
        catch(IOException ex) {
            Log.e("BT", ex.getMessage());
            return;
        }

        if(!sendMeasurementRequest()) {
            stopSelf();
        }

        try {
            while(bluetoothInput.available() <= 0) {
                // Wait for Bluetooth input here.
                Thread.sleep(20);
            }

            if(bluetoothInput.available() > 0) {
                Thread.sleep(200); // Wait for the full Bluetooth response to be available to the Android listener.
                readBluetoothInput(bluetoothInput);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String measurementString = "";
        if(latestMeasurement != null) {
            Location latestLocation = GPSHandler.getInstance().getLatestLocation();

            if(latestLocation != null) {
                latestMeasurement.setLatitude(latestLocation.getLatitude());
                latestMeasurement.setLongitude(latestLocation.getLongitude());
            }

            latestMeasurement.setUserSecret(deviceId);
            this.sendBroadcast(latestMeasurement.toString());
            measurementString = Measurement.toJSONString(latestMeasurement);
        }

        HTTPController.getInstance().sendPOST(measurementString);

        closeBluetoothSocket();
    }

    /**
     * Helper to effectively/cleanly close the bluetooth socket and it's connected streams.
     */
    private void closeBluetoothSocket() {
        try {
            if (bluetoothInput != null) {
                bluetoothInput.close();
                bluetoothInput = null;
            }

            if (bluetoothOutput != null) {
                bluetoothOutput.close();
                bluetoothOutput = null;
            }

            if (bluetoothSocket != null) {
                bluetoothSocket.close();
                bluetoothSocket = null;
            }
        } catch (IOException e) {
            Log.e("BT", e.getMessage());
        }
    }

    /**
     * Send the measurement request to the connected Snuffelneus device.
     * @return Whether sending the request was successful or not.
     */
    private boolean sendMeasurementRequest() {
        Log.i("REQ", "Sending measurement request");
        if (bluetoothSocket == null || bluetoothOutput == null) {
            return false;
        }

        try{
            bluetoothOutput.write(MEASURMENT_REQUEST_STRING.getBytes());
            bluetoothOutput.flush();
        } catch (IOException e) {
            Log.e("BT", e.getMessage());
            return false;
        }

        Log.i("BT", "Measurement request sent from sendMeasurementRequest()");
        return true;
    }

    /**
     * Method to read the Bluetooth input, and read it into a new Measurement object.
     * FORMAT: Result--sensor--temp--luchtvochtigheid--batterij1--batterij2--EOM
     *
     * @param bluetoothInput
     * @return
     */
    private Measurement readBluetoothInput(InputStream bluetoothInput) {
        Log.i("DECODE", "Reading Bluetooth input");
        Measurement newMeasurement = null;

        byte[] buffer = new byte[1024];
        int readOffset = 0;
        int totalMessageLength = 0;

        try {
            byte[] inputBuffer = new byte[256];
            int bytes = bluetoothInput.read(inputBuffer);
            Log.i("DECODE", "Bytes:" + bytes);

            System.arraycopy(inputBuffer, 0, buffer, readOffset, bytes);
            readOffset += bytes;
            totalMessageLength += bytes;

            String inputString = new String(buffer, 0, totalMessageLength);

            if (inputString.endsWith("EOM")) {
                Log.i("DECODE", inputString);
                latestMeasurement = Measurement.fromSnuffelneusString(inputString);
            }
        } catch (Exception e) {
            Log.e("BT", e.getMessage());
            closeBluetoothSocket();
            return null;
        }

        return newMeasurement;
    }

    /**
     * Wrapper method around LocalBroadcastManager sendBroadcast.
     * @param message Message to broadcast.
     */
    private void sendBroadcast(String message) {
        Intent intent = new Intent(SNUFFELNEUS);

        if (message != null && !message.isEmpty()) {
            intent.putExtra(SNUFFELNEUS, message);
        }
        broadcaster.sendBroadcast(intent);
    }

    /**
     * Create a new Bluetooth socket from the default Bluetooth adapter and the address set in the 'pair' screen.
     *
     * @param bluetoothAdapter Bluetooth adapter that should be used for this connection
     * @return Newly created bluetooth socket or null
     */
    private BluetoothSocket createBluetoothSocket(BluetoothAdapter bluetoothAdapter) {
        BluetoothSocket newBluetoothSocket = null;

        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            Log.i("BT", "Bluetooth adapter isn't activated/couldn't be found.");
            return null;
        }

        try {
            String bluetoothAddress = sharedPreferences.getString("BLUETOOTH_ADDRESS", "");

            if (bluetoothAddress == null || bluetoothAddress.isEmpty()) {
                Log.w("BT", "Empty Bluetooth address, returning from createBluetoothSocket");
                return null;
            }

            BluetoothDevice device = bluetoothAdapter.getRemoteDevice(bluetoothAddress);
            Method m = device.getClass().getMethod("createRfcommSocket", new Class[] { int.class });
            newBluetoothSocket = (BluetoothSocket) m.invoke(device, 1);
        } catch (Exception e) {
            Log.e("BT", e.getMessage());
            return null;
        }

        try {
            newBluetoothSocket.connect();
            Log.i("BT", "Successfully created Bluetooth socket.");
            return newBluetoothSocket;
        } catch (IOException ex) {

            Log.e("BT", ex.getMessage());
            closeBluetoothSocket();
        }

        return null;
    }
}
