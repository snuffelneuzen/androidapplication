package com.oxysocks.snuffelneus;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * TODO: Replace/update background receiver so it doesn't restart the backgroundReceiver every time we switch Activities.
 * Homescreen activity for this application
 */
public class HomeScreenActivity extends Activity {
    private static final int BAD_NO2_THRESHOLD = 1823;
    private static final int MEDIUM_NO2_THRESHOLD = 1100;

    private BackgroundBroadcastReceiver backgroundReceiver = null;
    private BroadcastReceiver broadcastReceiver;
    private TextView latestMeasurementView;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> outputStrings;
    private ArrayAdapter<String> outputAdapter;

    TextView temperatureView;
    TextView humidityView;
    TextView nitrogenView;
    ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        GPSHandler.getInstance().init(getApplicationContext());

        temperatureView = (TextView) findViewById(R.id.currentTemperature);
        humidityView = (TextView) findViewById(R.id.currentHumidity);
        nitrogenView = (TextView) findViewById(R.id.currentNO2);

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageResource(R.drawable.medium_color);

        initializeBroadcastReceiver();

        outputStrings = new ArrayList<String>();
        outputAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, outputStrings);

        ListView listView = (ListView) findViewById(R.id.outputView);
        listView.setAdapter(outputAdapter);

        outputStrings.add("Initialized!");
        outputAdapter.notifyDataSetChanged();

        sharedPreferences = getSharedPreferences("com.oxysocks.snuffelneus", MODE_PRIVATE);

        backgroundReceiver = new BackgroundBroadcastReceiver();
        backgroundReceiver.cancelBackgroundTask(getApplicationContext());
        backgroundReceiver.setBackgroundTask(getApplicationContext(), 0L);
    }

    /**
     * Method that initializes the internal broadcast receiver,
     * which is used for grabbing data from the BackgroundTask.
     */
    private void initializeBroadcastReceiver() {
        this.broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String data = intent.getExtras().getString(BackgroundTask.SNUFFELNEUS);

                if (data != null && !data.isEmpty()) {
                    Log.w("Receiver", data);
                    broadcastToScreen(data);
                    outputStrings.add(data);
                    outputAdapter.notifyDataSetChanged();
                }
            }
        };
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.pairDevice) {
            Intent intent = new Intent(this, PairActivity.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.mapView) {
            Intent intent = new Intent(this, MapViewActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Helper to update the screen from the data received by the BackgroundTask broadcast.
     * Updates using the broadcast Measurement data which is separated by whitespace.
     *
     * @param inputString Input from the Broadcast
     */
    private void broadcastToScreen(String inputString) {
        double currentNitrogen = 0.0;

        if(inputString.contains("Lat")) {
            return;
        }

        String split[] = inputString.split(" ");
        temperatureView.setText(split[0]);
        humidityView.setText(split[1]);
        nitrogenView.setText(split[2]);

        try {
            currentNitrogen = Double.parseDouble(split[2]);
        } catch(NumberFormatException nfe) {
            Log.i("HOME", "Could not parse current nitrogen value.");
        }

        if(currentNitrogen >= BAD_NO2_THRESHOLD) {
            imageView.setImageResource(R.drawable.bad_color);
        }
        else if(currentNitrogen < BAD_NO2_THRESHOLD && currentNitrogen >= MEDIUM_NO2_THRESHOLD) {
            imageView.setImageResource(R.drawable.medium_color);
        }
        else {
            imageView.setImageResource(R.drawable.good_color);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((broadcastReceiver), new IntentFilter(BackgroundTask.SNUFFELNEUS));
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onStop();
    }


}
