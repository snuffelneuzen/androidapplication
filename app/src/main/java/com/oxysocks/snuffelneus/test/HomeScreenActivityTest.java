package com.oxysocks.snuffelneus.test;

import com.oxysocks.snuffelneus.HomeScreenActivity;
import com.oxysocks.snuffelneus.R;

import android.content.Intent;
import android.test.ActivityUnitTestCase;
import android.test.suitebuilder.annotation.MediumTest;
import android.widget.Button;

/**
 * Tests for the 'HomeScreenActivity'
 * @see http://developer.android.com/reference/android/test/ActivityUnitTestCase.html
 */
public class HomeScreenActivityTest extends ActivityUnitTestCase<HomeScreenActivity> {
    private Intent launchIntent;

    public HomeScreenActivityTest(Class<HomeScreenActivity> activityClass) { super(activityClass); }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        launchIntent = new Intent(getInstrumentation() .getTargetContext(), HomeScreenActivity.class);
        startActivity(launchIntent, null, null);
    }
}
