package com.oxysocks.snuffelneus.test;

import android.test.InstrumentationTestCase;
import android.test.suitebuilder.annotation.SmallTest;

import com.oxysocks.snuffelneus.HTTPController;
import com.oxysocks.snuffelneus.Measurement;

/**
 * 'On-phone' unittests for the Snuffelneus application.
 * @see http://rexstjohn.com/unit-testing-with-android-studio/
 */
public class GeneralComponentTest extends InstrumentationTestCase {

    /**
     * Method to test the Measurement JSON serializer "Measurement.toJSONString()".
     * @throws java.lang.Exception on test failure.
     */
    @SmallTest
    public void testMeasurementSerializer() throws Exception {
        Measurement measurement = getTestMeasurement();

        assertEquals("JSON serialization must match input.",
                "{\"Secret\":\"UNIT_TEST\",\"Values\":[{\"Sensor\":\"NO2\",\"Value\":150},{\"Sensor\":\"temp\",\"Value\":23},{\"Sensor\":\"hum\",\"Value\":23}],\"Latitude\":50,\"Longitude\":50}",
                Measurement.toJSONString(measurement));
    }

    /**
     * Method to test the HTTPController 'sendPost()' method.
     * @throws java.lang.Exception on test failure.
     */
    @SmallTest
    public void testServerPOST() throws Exception {
        Measurement measurement = getTestMeasurement();
        boolean completed = HTTPController.getInstance().sendPOST(Measurement.toJSONString(measurement));
        assertEquals(true, completed);
    }

    /**
     * Helper method to get a full test measurement, for reliable data accross tests.
     * @return
     */
    private Measurement getTestMeasurement() {
        Measurement measurement = new Measurement();

        measurement.setUserSecret("UNIT_TEST");

        measurement.setLatitude(50.0);
        measurement.setLongitude(50.0);

        measurement.setTemperature(23.0);
        measurement.setHumidity(23.0);
        measurement.setSensorValue(150.0);

        measurement.setBatteryOne(1.5);
        measurement.setBatteryOne(2.0);

        return measurement;
    }
}
