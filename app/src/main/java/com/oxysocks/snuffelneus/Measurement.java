package com.oxysocks.snuffelneus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Measurement {
    private double sensorValue;
    private double temperature;
    private double humidity;
    private double batteryOne;
    private double batteryTwo;
    private double latitude;
    private double longitude;
    private String userSecret;

    public Measurement() {
        this.sensorValue = this.temperature = this.humidity = this.batteryOne = 0.0;
        this.batteryTwo = this.latitude = this.longitude = 0.0;
        userSecret = "";
    }

    /**
     * Helper to convert a Snuffelneus Bluetooth input string to a new Measurement instance
     *
     * @param inputString Input string from the Snuffelneus PCB
     * @return Measurement built from input.
     */
    public static Measurement fromSnuffelneusString(String inputString) {
        Measurement newMeasurement = new Measurement();

        String split[] = inputString.split("--");

        double sensorValue = Double.parseDouble(split[1]);
        double temperature = Double.parseDouble(split[2]);
        double humidity  = Double.parseDouble(split[3]);
        double batteryOne = Double.parseDouble(split[4]);
        double batteryTwo  = Double.parseDouble(split[5]);

        newMeasurement.setSensorValue(sensorValue);
        newMeasurement.setTemperature(temperature);
        newMeasurement.setBatteryOne(batteryOne);
        newMeasurement.setBatteryTwo(batteryTwo);
        newMeasurement.setHumidity(humidity);

        return newMeasurement;
    }

    /**
     * Helper to convert a Measurement object to a String suitable for HTTP POST'ing to the API.
     * @param measurement Input Measurement instance that should be converted
     * @return Encoded Measurement as JSON
     */
    public static String toJSONString(final Measurement measurement) {
        JSONObject measurementObject = new JSONObject();
        JSONArray sensorArray = new JSONArray();
        JSONObject tempObj = new JSONObject();

        try {
            tempObj.put("Sensor", "NO2");
            tempObj.put("Value", measurement.getSensorValue());
            sensorArray.put(tempObj);
            tempObj = new JSONObject();
            tempObj.put("Sensor", "temp");
            tempObj.put("Value", measurement.getTemperature());
            sensorArray.put(tempObj);
            tempObj = new JSONObject();
            tempObj.put("Sensor", "hum");
            tempObj.put("Value", measurement.getHumidity());
            sensorArray.put(tempObj);
            measurementObject.put("Values", sensorArray);
            measurementObject.put("Secret", measurement.getUserSecret());
            measurementObject.put("Longitude", measurement.getLongitude());
            measurementObject.put("Latitude", measurement.getLatitude());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return measurementObject.toString();
    }

    @Override
    public String toString() {
        return this.getTemperature() + "\u2103 " + this.getHumidity() + "% " + this.getSensorValue();
    }

    public double getSensorValue() {
        return sensorValue;
    }

    public void setSensorValue(double sensorValue) {
        this.sensorValue = sensorValue;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public double getBatteryOne() {
        return batteryOne;
    }

    public void setBatteryOne(double batteryOne) {
        this.batteryOne = batteryOne;
    }

    public double getBatteryTwo() {
        return batteryTwo;
    }

    public void setBatteryTwo(double batteryTwo) {
        this.batteryTwo = batteryTwo;
    }



    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getUserSecret() {
        return userSecret;
    }

    public void setUserSecret(String userSecret) {
        this.userSecret = userSecret;
    }
}
