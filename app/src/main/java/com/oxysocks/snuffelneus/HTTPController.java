package com.oxysocks.snuffelneus;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

/**
 * Class that is responsible for sending HTTP requests to the Snuffelneus Web API.
 */
public class HTTPController {
    //TODO: Replace with Application setting/make this configurable!
    private final String API_ADDRESS = "http://snuffelneus.rubberducky.eu/api/values";

    private static HTTPController instance = null;

    private HttpClient httpClient;

    protected HTTPController() { httpClient = new DefaultHttpClient(); }

    /**
     * Get this HTTPController's instance so we don't have to keep initializing the HttpClient.
     * @return (Singleton) HTTPController instance.
     */
    public static HTTPController getInstance() {
        if(instance == null) {
            instance = new HTTPController();
        }

        return instance;
    }

    /**
     * By definition, this method is synchronous. However, it is run in the background.
     * Blocking effects should be negligible unless measurement interval is really low or connection quality is bad.
     *
     * @param data The input data that should be sent to the server.
     */
    public boolean sendPOST(final String inputString) {
        boolean completed = false;

        try {
            HttpPost httpPost = new HttpPost(API_ADDRESS);
            StringEntity stringEntity = new StringEntity(inputString);

            httpPost.setEntity(stringEntity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            Log.i("POST", EntityUtils.toString(httpPost.getEntity()));
            HttpResponse httpResponse = httpClient.execute(httpPost);
            Log.i("RESP", EntityUtils.toString(httpResponse.getEntity()));

            httpResponse.getEntity().consumeContent();
            completed = true;
        } catch (Exception e) {
            Log.e("HTTPController", e.getMessage());
        }

        return completed;
    }
}
