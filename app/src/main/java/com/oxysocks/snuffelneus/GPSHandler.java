package com.oxysocks.snuffelneus;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Singleton that handles GPS for the Snuffelneus application.
 * Responsible for grabbing the latest location in the background using the 'UPDATE_TIME' as it's updating interval.
 *
 * The latest location can be acquired using the getLatestLocation() function, which is the core of this class.
 * It provides synchronised access to the latest, best location that could be determined by the Android LocationManager.
 */
public class GPSHandler {
    private static final int TWO_MINUTES = 1000 * 60 * 2;
    private static final int UPDATE_TIME = 1000 * 60; // In milliseconds

    private static LocationManager locationManager;
    private LocationListener locationListener;

    private Location latestLocation;
    private Context context = null;

    private static GPSHandler instance = null;

    /**
     * Public access to the GPS handler instance. Singleton.
     * @return
     */
    public static GPSHandler getInstance() {
        if(instance == null) {
            instance = new GPSHandler();
        }

        return instance;
    }

    private GPSHandler() { }

    /**
     * Initialization for the GPS handler. Mainly used to grab the Application context as it is required for
     * getting the Android Location Service.
     *
     * @param context The context in which this GPSHandler should be used
     */
    public void init(Context context) {
        if(this.context == null) {
            this.context = context.getApplicationContext();

            locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

            if (locationManager == null) {
                Log.e("GPS", "Unable to access Location Manager.");
            }

            Location lastNetworkLocation = locationManager .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            Location lastGPSLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            if (isBetterLocation(lastNetworkLocation, lastGPSLocation)) {
                latestLocation = lastNetworkLocation;
            }
            else {
                latestLocation = lastGPSLocation;
            }

            locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    Log.e("GPS", "Location changed.");
                    if(isBetterLocation(location, latestLocation)) {
                        setLatestLocation(location);
                    }
                }

                @Override
                public void onStatusChanged(String s, int i, Bundle bundle) {}

                @Override
                public void onProviderEnabled(String s) { }

                @Override
                public void onProviderDisabled(String s) { }
            };

            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_TIME, 0, locationListener);
        }
    }



    /**
     * Grabs the latest, best, location that could be determined at the end of the last update interval.
     * Provides synchronised access so threads should be able to safely access it.
     *
     * @return The latest, best, location that could be determined at the end of the last update interval set by the programmer.
     */
    public synchronized Location getLatestLocation() {
        return latestLocation;
    }

    public synchronized  void setLatestLocation(Location location) { this.latestLocation = location; }

    /**
     * Helper to determine if one location is better than the other one.
     * @param location The new location for which it's quality must be compared.
     * @param currentBestLocation The location that is currently the 'best' estimate.
     * @return Boolean that contains whether the location is better than the currently best location.
     */
    private boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (location == null) {
            Log.e("isBetterLocation", "New location is null");
            return false;
        }

        if (currentBestLocation == null) {
            return true;
        }

        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        if (isSignificantlyNewer) {
            return true;
        } else if (isSignificantlyOlder) {
            return false;
        }

        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }

        return false;
    }

    /**
     * Helper to see if two providers are the same.
     * @param firstProvider First provider for comparison
     * @param secondProvider Second provider for comparison
     * @return Whether the providers are the same.
     */
    private boolean isSameProvider(String firstProvider, String secondProvider) {
        if (firstProvider == null) {
            return secondProvider == null;
        }
        return firstProvider.equals(secondProvider);
    }
}
