package com.oxysocks.snuffelneus;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Class responsible for starting the BackgroundTask using the Android AlarmManager.
 * @see android.content.BroadcastReceiver
 */
public class BackgroundBroadcastReceiver extends BroadcastReceiver
{
    // The interval that should used for 'triggering' the background task in milliseconds.
    // a sensible default value is provided. (5 * 60 * 1000 = every 5 minutes)
    public static final long SERVICE_INTERVAL = 1 * 30 * 1000;

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, BackgroundTask.class));
    }


    /**
     * Cancels the BackgroundTask by telling the AlarmManager to remove it from it's list of tasks.
     *
     * @param context The current application context.
     */
    public void cancelBackgroundTask(Context context) {
        Intent intent = new Intent(context, BackgroundBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }

    /**
     * Check if the Alarm running this class is already running.
     *
     * @param context The current application context.
     * @return Whether the background task is already running.
     */
    public boolean alarmIsRunning(Context context) {
        Intent i = new Intent(context, BackgroundBroadcastReceiver.class);
        boolean alarmUp = (PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_NO_CREATE) != null);
        return alarmUp;
    }

    /**
     * Activates the BackgroundTask, by triggering the Android Alarm manager using this class.
     * Calls the onReceive method from here, which starts the BackgroundTask background service.
     *
     * @param context The current application context.
     * @param offset The offset after which the AlarmManager should trigger the run.
     */
    public void setBackgroundTask(Context context, long offset) {
        Intent i = new Intent(context, BackgroundBroadcastReceiver.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, i, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC,System.currentTimeMillis() + offset, SERVICE_INTERVAL, sender);
    }
}
