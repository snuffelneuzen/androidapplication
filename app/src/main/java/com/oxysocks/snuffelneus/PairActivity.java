package com.oxysocks.snuffelneus;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Activity that handles pairing to an existing Snuffelneus, that is NOT currently paired.
 */
public class PairActivity extends Activity {
    private BluetoothAdapter bluetoothAdapter = null;
    private BluetoothBroadcastReceiver bluetoothBroadcastReceiver = null;
    private List<String> devices;
    private List<String> pairedDevices;
    private List<String> deviceBuffer;
    private ArrayAdapter<String> deviceListAdapter = null;
    private ArrayAdapter<String> pairedDeviceListAdapter = null;
    private IntentFilter intentFilter;
    private SharedPreferences sharedPreferences;

    private boolean firstScan;

    private int REQUEST_ENABLE_BT = 12;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pair);
        sharedPreferences = getSharedPreferences("com.oxysocks.snuffelneus", MODE_PRIVATE);

        firstScan = true;
        devices = new ArrayList<String>();
        pairedDevices = new ArrayList<String>();
        deviceBuffer = new ArrayList<String>();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        setResult(RESULT_CANCELED);

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        }

        bluetoothBroadcastReceiver = new BluetoothBroadcastReceiver();

        intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
        intentFilter.addAction(BluetoothDevice.ACTION_BOND_STATE_CHANGED);
        registerReceiver(bluetoothBroadcastReceiver, intentFilter);

        initializeDeviceList();

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        startBluetoothDiscovery();
    }

    /**
     * Method that initializes the list of devices available in this activity.
     */
    private void initializeDeviceList() {
        ListView deviceList = (ListView) findViewById(R.id.deviceList);

        deviceListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, devices);

        deviceList.setAdapter(deviceListAdapter);
        deviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String viewText = (String) ((TextView) view).getText();

                //TODO: Clean this one up, see if we can make it more sane (-17, really?)
                String bluetoothAddress = viewText.substring(viewText.length() - 17);

                if (bluetoothAddress != null) {
                    bluetoothAdapter.cancelDiscovery();
                    Log.i("PAIR", "Clicked unpaired device with Bluetooth address: " + bluetoothAddress);

                    try {

                        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(bluetoothAddress);
                        Method m = device.getClass().getMethod("createRfcommSocket", new Class[] {int.class});
                        BluetoothSocket btSocket = (BluetoothSocket) m.invoke(device, 1);
                        btSocket.connect();
                    } catch (Exception e) {
                        Log.e("Bluetooth", e.getMessage());
                    }
                }
            }
        });

//        ListView pairedDeviceList = (ListView) findViewById(R.id.pairedDeviceList);
//
//        pairedDeviceListAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, pairedDevices);
//        pairedDeviceList.setAdapter(pairedDeviceListAdapter);
//        deviceList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                String viewText = (String) ((TextView) view).getText();
//                String bluetoothAddress = viewText.substring(viewText.length() - 17);
//            }
//        });

        }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_ENABLE_BT) {
            if(resultCode == RESULT_OK) {
                Log.i("BT", "Enabled bluetooth.");
                startBluetoothDiscovery();
            }
            else {
                Log.i("BT", "Couldn't enable Bluetooth.");
                Toast.makeText(getApplicationContext(), "Bluetooth is benodigd voor het gebruik van de Snuffelneus.", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    /**
     * Method that starts Bluetooth discovery,
     * if the current adapter is not null and it is enabled.
     */
    private void startBluetoothDiscovery() {
        if (bluetoothAdapter != null && bluetoothAdapter.isEnabled()) {
            if(!bluetoothAdapter.startDiscovery()) {
                Log.e("Bluetooth", "Unable to start bluetooth discovery.");
                return;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (bluetoothAdapter != null) {
            bluetoothAdapter.cancelDiscovery();
        }
        try {
            unregisterReceiver(bluetoothBroadcastReceiver);
        } catch (Exception e) {
            Log.e("PairActivity", e.getMessage());
        }
    }

    /**
     * Internal class that is responsible for handling the bluetooth broadcasts.
     * Responsible for receiving
     */
    private class BluetoothBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();

            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if (device.getBondState() != BluetoothDevice.BOND_BONDED) {
                    if (device.getName() != null && device.getName().startsWith("snuffelneus")) {
                        String deviceName = device.getName() + "\n"+ device.getAddress();

                        if(firstScan) {
                            devices.add(deviceName);
                            deviceListAdapter.notifyDataSetChanged();
                        }
                        else {
                            deviceBuffer.add(deviceName);
                        }
                    }
                }
            }

            if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                Log.i("BT", "Bluetooth discovery finished.");

                if(!firstScan) {
                    updateFromDeviceBuffer();
                }
                else {
                    firstScan = false;
                }

                startBluetoothDiscovery();
            }

            if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);

                if(device.getBondState() == BluetoothDevice.BOND_BONDED) {
                    unregisterReceiver(this);

                    String newAddress = device.getAddress();
                    sharedPreferences.edit().putString("BLUETOOTH_ADDRESS", newAddress).commit();
                    Log.i("PAIR", "Coupled with new device at: " + newAddress);

                    finish();
                }
            }
        }
    }

    /**
     * Helper to update the device list from the buffer.
     */
    private void updateFromDeviceBuffer() {
        devices.clear();
        devices.addAll(deviceBuffer);
        deviceBuffer.clear();
        deviceListAdapter.notifyDataSetChanged();
    }

}
